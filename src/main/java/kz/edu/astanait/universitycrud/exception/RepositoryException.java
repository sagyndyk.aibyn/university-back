package kz.edu.astanait.universitycrud.exception;

public class RepositoryException extends RuntimeException {

    public RepositoryException(String message){
        super(message);
    }
}
