package kz.edu.astanait.universitycrud.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "specialities")
@Data
public class Speciality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
}
