package kz.edu.astanait.universitycrud.service.serviceImpl;

import kz.edu.astanait.universitycrud.dto.request.SubjectDtoRequest;
import kz.edu.astanait.universitycrud.exception.CustomNotFoundException;
import kz.edu.astanait.universitycrud.exception.ExceptionDescription;
import kz.edu.astanait.universitycrud.exception.RepositoryException;
import kz.edu.astanait.universitycrud.model.Speciality;
import kz.edu.astanait.universitycrud.model.Subject;
import kz.edu.astanait.universitycrud.repository.SubjectRepository;
import kz.edu.astanait.universitycrud.service.serviceInterface.SpecialityService;
import kz.edu.astanait.universitycrud.service.serviceInterface.SubjectService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Log4j2
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;
    private final SpecialityService specialityService;

    public SubjectServiceImpl(SubjectRepository subjectRepository, SpecialityService specialityService) {
        this.subjectRepository = subjectRepository;
        this.specialityService = specialityService;
    }

    @Override
    public Optional<Subject> getById(Long id) {
        return this.subjectRepository.findById(id);
    }

    @Override
    public Subject getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Subject", "id", id)));
    }

    @Override
    public List<Subject> getAll() {
        return this.subjectRepository.findAll();
    }

    @Override
    public void create(SubjectDtoRequest subjectDtoRequest) {
        Subject subject = new Subject();

        List<Speciality> specialityList = new ArrayList<>();

        subject.setName(subjectDtoRequest.getName());

        subjectDtoRequest.getSpeciality().forEach(v -> {
            specialityList.add(this.specialityService.getByIdThrowException(v));
        });

        subject.setSpeciality(specialityList);

        try{
            this.subjectRepository.save(subject);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "student"));
        }
    }

    @Override
    public void update(SubjectDtoRequest subjectDtoRequest, Long id) {
        Subject subject = this.getByIdThrowException(id);

        List<Speciality> specialityList = new ArrayList<>();

        if(Strings.isNotBlank(subjectDtoRequest.getName())) subject.setName(subjectDtoRequest.getName());


        subjectDtoRequest.getSpeciality().forEach(v -> {
            specialityList.add(this.specialityService.getByIdThrowException(v));
        });

        subject.setSpeciality(specialityList);


        try{
            this.subjectRepository.save(subject);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "updating", "subject"));
        }
    }

    @Override
    public void delete(Long id) {
        Subject subject = this.getByIdThrowException(id);

        try{
            this.subjectRepository.delete(subject);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "deleting", "subject"));
        }

    }
}
