package kz.edu.astanait.universitycrud.service.serviceImpl;

import kz.edu.astanait.universitycrud.dto.request.SpecialityDtoRequest;
import kz.edu.astanait.universitycrud.exception.CustomNotFoundException;
import kz.edu.astanait.universitycrud.exception.ExceptionDescription;
import kz.edu.astanait.universitycrud.exception.RepositoryException;
import kz.edu.astanait.universitycrud.model.Speciality;
import kz.edu.astanait.universitycrud.repository.SpecialityRepository;
import kz.edu.astanait.universitycrud.service.serviceInterface.SpecialityService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Log4j2
public class SpecialityServiceImpl implements SpecialityService {

    private final SpecialityRepository specialityRepository;

    public SpecialityServiceImpl(SpecialityRepository specialityRepository) {
        this.specialityRepository = specialityRepository;
    }

    @Override
    public Optional<Speciality> getById(Long id) {
        return this.specialityRepository.findById(id);
    }

    @Override
    public Speciality getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Speciality", "id", id)));
    }

    @Override
    public List<Speciality> getAll() {
        return this.specialityRepository.findAll();
    }

    @Override
    public void create(SpecialityDtoRequest specialityDtoRequest) {
        Speciality speciality = new Speciality();

        speciality.setName(specialityDtoRequest.getName());

        try{
            this.specialityRepository.save(speciality);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "speciality"));
        }
    }

    @Override
    public void update(SpecialityDtoRequest specialityDtoRequest, Long id) {
        Speciality speciality = this.getByIdThrowException(id);

        if(Strings.isNotBlank(specialityDtoRequest.getName())) speciality.setName(specialityDtoRequest.getName());

        try{
            this.specialityRepository.save(speciality);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "updating", "speciality"));
        }

    }

    @Override
    public void delete(Long id) {
        Speciality speciality = this.getByIdThrowException(id);

        try{
            this.specialityRepository.delete(speciality);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "deleting", "speciality"));
        }

    }
}
