package kz.edu.astanait.universitycrud.service.serviceImpl;

import kz.edu.astanait.universitycrud.dto.request.GroupDtoRequest;
import kz.edu.astanait.universitycrud.exception.CustomNotFoundException;
import kz.edu.astanait.universitycrud.exception.ExceptionDescription;
import kz.edu.astanait.universitycrud.exception.RepositoryException;
import kz.edu.astanait.universitycrud.model.Group;
import kz.edu.astanait.universitycrud.repository.GroupRepository;
import kz.edu.astanait.universitycrud.service.serviceInterface.GroupService;
import kz.edu.astanait.universitycrud.service.serviceInterface.SpecialityService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Log4j2
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final SpecialityService specialityService;

    public GroupServiceImpl(GroupRepository groupRepository, SpecialityService specialityService) {
        this.groupRepository = groupRepository;
        this.specialityService = specialityService;
    }

    @Override
    public Optional<Group> getById(Long id) {
        return this.groupRepository.findById(id);
    }

    @Override
    public Group getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Group", "id", id)));
    }

    @Override
    public List<Group> getAll() {
        return this.groupRepository.findAll();
    }

    @Override
    public void create(GroupDtoRequest groupDtoRequest) {
        Group group = new Group();

        group.setName(groupDtoRequest.getName());
        group.setSpeciality(this.specialityService.getByIdThrowException(groupDtoRequest.getSpeciality()));

        try{
            this.groupRepository.save(group);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "group"));
        }

    }

    @Override
    public void update(GroupDtoRequest groupDtoRequest, Long id) {
        Group group = this.getByIdThrowException(id);

        if(Strings.isNotBlank(groupDtoRequest.getName())) group.setName(groupDtoRequest.getName());
        if(Objects.nonNull(groupDtoRequest.getSpeciality())) group.setSpeciality(this.specialityService.getByIdThrowException(groupDtoRequest.getSpeciality()));


        try{
            this.groupRepository.save(group);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "updating", "group"));
        }
    }

    @Override
    public void delete(Long id) {

        Group group = this.getByIdThrowException(id);

        try {
            this.groupRepository.delete(group);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "deleting", "group"));
        }
    }
}