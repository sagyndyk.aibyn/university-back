package kz.edu.astanait.universitycrud.service.serviceInterface;

import kz.edu.astanait.universitycrud.dto.request.StudentDtoRequest;
import kz.edu.astanait.universitycrud.model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Optional<Student> getById(Long id);

    Student getByIdThrowException(Long id);

    List<Student> getAll();

    void create(StudentDtoRequest studentDtoRequest);

    void update(StudentDtoRequest studentDtoRequest, Long id);

    void delete(Long id);
}
