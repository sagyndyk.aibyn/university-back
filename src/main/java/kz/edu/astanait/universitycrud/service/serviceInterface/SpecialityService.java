package kz.edu.astanait.universitycrud.service.serviceInterface;

import kz.edu.astanait.universitycrud.dto.request.SpecialityDtoRequest;
import kz.edu.astanait.universitycrud.model.Speciality;

import java.util.List;
import java.util.Optional;

public interface SpecialityService {

    Optional<Speciality> getById(Long id);

    Speciality getByIdThrowException(Long id);

    List<Speciality> getAll();

    void create(SpecialityDtoRequest specialityDtoRequest);

    void update(SpecialityDtoRequest specialityDtoRequest, Long id);

    void delete(Long id);
}
