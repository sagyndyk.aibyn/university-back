package kz.edu.astanait.universitycrud.service.serviceInterface;

import kz.edu.astanait.universitycrud.dto.request.SubjectDtoRequest;
import kz.edu.astanait.universitycrud.model.Subject;

import java.util.List;
import java.util.Optional;

public interface SubjectService {

    Optional<Subject> getById(Long id);

    Subject getByIdThrowException(Long id);

    List<Subject> getAll();

    void create(SubjectDtoRequest subjectDtoRequest);

    void update(SubjectDtoRequest subjectDtoRequest, Long id);

    void delete(Long id);
}
