package kz.edu.astanait.universitycrud.service.serviceInterface;

import kz.edu.astanait.universitycrud.dto.request.GroupDtoRequest;
import kz.edu.astanait.universitycrud.model.Group;

import java.util.List;
import java.util.Optional;

public interface GroupService {

    Optional<Group> getById(Long id);

    Group getByIdThrowException(Long id);

    List<Group> getAll();

    void create(GroupDtoRequest groupDtoRequest);

    void update(GroupDtoRequest groupDtoRequest, Long id);

    void delete(Long id);
}
