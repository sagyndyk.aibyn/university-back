package kz.edu.astanait.universitycrud.service.serviceInterface;

import kz.edu.astanait.universitycrud.dto.request.TeacherDtoRequest;
import kz.edu.astanait.universitycrud.model.Teacher;

import java.util.List;
import java.util.Optional;

public interface TeacherService {

    Optional<Teacher> getById(Long id);

    Teacher getByIdThrowException(Long id);

    List<Teacher> getAll();

    void create(TeacherDtoRequest teacherDtoRequest);

    void update(TeacherDtoRequest teacherDtoRequest, Long id);

    void delete(Long id);
}
