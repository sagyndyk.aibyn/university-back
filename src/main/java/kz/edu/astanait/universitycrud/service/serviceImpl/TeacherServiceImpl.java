package kz.edu.astanait.universitycrud.service.serviceImpl;

import kz.edu.astanait.universitycrud.dto.request.TeacherDtoRequest;
import kz.edu.astanait.universitycrud.exception.CustomNotFoundException;
import kz.edu.astanait.universitycrud.exception.ExceptionDescription;
import kz.edu.astanait.universitycrud.exception.RepositoryException;
import kz.edu.astanait.universitycrud.model.Subject;
import kz.edu.astanait.universitycrud.model.Teacher;
import kz.edu.astanait.universitycrud.repository.TeacherRepository;
import kz.edu.astanait.universitycrud.service.serviceInterface.SubjectService;
import kz.edu.astanait.universitycrud.service.serviceInterface.TeacherService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Log4j2
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;
    private final SubjectService subjectService;

    public TeacherServiceImpl(TeacherRepository teacherRepository, SubjectService subjectService) {
        this.teacherRepository = teacherRepository;
        this.subjectService = subjectService;
    }

    @Override
    public Optional<Teacher> getById(Long id) {
        return this.teacherRepository.findById(id);
    }

    @Override
    public Teacher getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Teacher", "id", id)));

    }

    @Override
    public List<Teacher> getAll() {
        return this.teacherRepository.findAll();
    }

    @Override
    public void create(TeacherDtoRequest teacherDtoRequest) {
        Teacher teacher = new Teacher();

        List<Subject> subjectList = new ArrayList<>();

        teacher.setName(teacherDtoRequest.getName());
        teacher.setSurname(teacherDtoRequest.getSurname());
        teacherDtoRequest.getSubject().forEach(v -> {
           subjectList.add(this.subjectService.getByIdThrowException(v));
        });

        teacher.setSubject(subjectList);

        try{
            this.teacherRepository.save(teacher);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "teacher"));
        }
    }

    @Override
    public void update(TeacherDtoRequest teacherDtoRequest, Long id) {
        Teacher teacher = this.getByIdThrowException(id);

        List<Subject> subjectList = new ArrayList<>();

        if(Strings.isNotBlank(teacherDtoRequest.getName())) teacher.setName(teacherDtoRequest.getName());
        if(Strings.isNotBlank(teacherDtoRequest.getSurname())) teacher.setSurname(teacherDtoRequest.getSurname());

        teacherDtoRequest.getSubject().forEach(v -> {
            subjectList.add(this.subjectService.getByIdThrowException(id));
        });

        teacher.setSubject(subjectList);

        try{
            this.teacherRepository.save(teacher);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "updating", "teacher"));
        }
    }

    @Override
    public void delete(Long id) {
        Teacher teacher = this.getByIdThrowException(id);

        try{
            this.teacherRepository.delete(teacher);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "deleting", "teacher"));
        }

    }
}
