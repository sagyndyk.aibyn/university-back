package kz.edu.astanait.universitycrud.service.serviceImpl;

import kz.edu.astanait.universitycrud.dto.request.StudentDtoRequest;
import kz.edu.astanait.universitycrud.exception.CustomNotFoundException;
import kz.edu.astanait.universitycrud.exception.ExceptionDescription;
import kz.edu.astanait.universitycrud.exception.RepositoryException;
import kz.edu.astanait.universitycrud.model.Student;
import kz.edu.astanait.universitycrud.repository.StudentRepository;
import kz.edu.astanait.universitycrud.service.serviceInterface.GroupService;
import kz.edu.astanait.universitycrud.service.serviceInterface.StudentService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Log4j2
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    private final GroupService groupService;

    public StudentServiceImpl(StudentRepository studentRepository, GroupService groupService) {
        this.studentRepository = studentRepository;
        this.groupService = groupService;
    }

    @Override
    public Optional<Student> getById(Long id) {
        return this.studentRepository.findById(id);
    }

    @Override
    public Student getByIdThrowException(Long id) {
        return this.getById(id)
                .orElseThrow(() -> new CustomNotFoundException
                        (String.format(ExceptionDescription.CustomNotFoundException, "Student", "id", id)));
    }

    @Override
    public List<Student> getAll() {
        return this.studentRepository.findAll();
    }

    @Override
    public void create(StudentDtoRequest studentDtoRequest) {
        Student student = new Student();

        student.setName(studentDtoRequest.getName());
        student.setSurname(studentDtoRequest.getSurname());
        student.setGroup(this.groupService.getByIdThrowException(studentDtoRequest.getGroup()));



        try{
            this.studentRepository.save(student);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "creating", "student"));
        }
    }

    @Override
    public void update(StudentDtoRequest studentDtoRequest, Long id) {
        Student student = this.getByIdThrowException(id);

        if(Strings.isNotBlank(studentDtoRequest.getName())) student.setName(studentDtoRequest.getName());
        if(Strings.isNotBlank(studentDtoRequest.getSurname())) student.setSurname(studentDtoRequest.getSurname());
        if(Objects.nonNull(studentDtoRequest.getGroup())) student.setGroup(this.groupService.getByIdThrowException(studentDtoRequest.getGroup()));

        try{
            this.studentRepository.save(student);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "updating", "student"));
        }

    }

    @Override
    public void delete(Long id) {
        Student student = this.getByIdThrowException(id);

        try{
            this.studentRepository.delete(student);
        }catch (Exception e){
            log.error(e);
            throw new RepositoryException(String
                    .format(ExceptionDescription.RepositoryException, "deleting", "student"));
        }

    }
}
