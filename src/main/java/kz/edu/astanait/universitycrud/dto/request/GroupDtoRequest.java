package kz.edu.astanait.universitycrud.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GroupDtoRequest {

    @NotNull(message = "The name was not specified.")
    private String name;

    @NotNull(message = "The speciality was not specified.")
    private Long speciality;
}
