package kz.edu.astanait.universitycrud.dto.response;

import lombok.Data;

@Data
public class SubjectDtoResponse {

    private Long id;

    private String name;

    private SpecialityDtoResponse specialityDtoResponse;
}
