package kz.edu.astanait.universitycrud.dto.response;

import lombok.Data;

@Data
public class StudentDtoResponse {

    private Long id;

    private String name;

    private String surname;

    private GroupDtoResponse groupDtoResponse;
}
