package kz.edu.astanait.universitycrud.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TeacherDtoRequest {

    @NotNull(message = "The name was not specified.")
    private String name;

    @NotNull(message = "The surname was not specified.")
    private String surname;

    @NotNull(message = "The subject was not specified.")
    private List<Long> subject;
}
