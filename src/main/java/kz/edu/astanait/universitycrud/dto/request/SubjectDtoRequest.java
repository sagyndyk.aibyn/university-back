package kz.edu.astanait.universitycrud.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class SubjectDtoRequest {

    @NotNull(message = "The name was not specified.")
    private String name;

    @NotNull(message = "The speciality was not specified.")
    private List<Long> speciality;
}
