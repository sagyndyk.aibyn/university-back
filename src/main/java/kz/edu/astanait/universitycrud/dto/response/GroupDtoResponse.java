package kz.edu.astanait.universitycrud.dto.response;

import lombok.Data;

@Data
public class GroupDtoResponse {

    private Long id;

    private String name;

    private SpecialityDtoResponse speciality;
}
