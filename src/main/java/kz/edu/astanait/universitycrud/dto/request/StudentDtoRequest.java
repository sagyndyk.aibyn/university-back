package kz.edu.astanait.universitycrud.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StudentDtoRequest {

    @NotNull(message = "The name was not specified.")
    private String name;

    @NotNull(message = "The surname was not specified.")
    private String surname;

    @NotNull(message = "The group was not specified")
    private Long group;
}
