package kz.edu.astanait.universitycrud.dto.response;

import lombok.Data;


@Data
public class SpecialityDtoResponse {

    private Long id;

    private String name;
}
