package kz.edu.astanait.universitycrud.dto.response;

import lombok.Data;

@Data
public class TeacherDtoResponse {

    private Long id;

    private String name;

    private String surname;

    private SubjectDtoResponse subjectDtoResponse;
}
