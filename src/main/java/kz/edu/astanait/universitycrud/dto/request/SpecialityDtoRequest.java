package kz.edu.astanait.universitycrud.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SpecialityDtoRequest {

    @NotNull(message = "The name was not specified.")
    private String name;
}
