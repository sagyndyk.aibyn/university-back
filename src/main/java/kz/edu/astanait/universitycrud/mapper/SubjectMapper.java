package kz.edu.astanait.universitycrud.mapper;

import kz.edu.astanait.universitycrud.dto.response.SubjectDtoResponse;
import kz.edu.astanait.universitycrud.model.Subject;
import org.apache.logging.log4j.util.Strings;

public class SubjectMapper {

    public static SubjectDtoResponse subjectToDto(Subject subject){
        SubjectDtoResponse subjectDtoResponse = new SubjectDtoResponse();

        subjectDtoResponse.setId(subject.getId());
        if(Strings.isNotBlank(subject.getName())) subjectDtoResponse.setName(subject.getName());
        return subjectDtoResponse;
    }
}
