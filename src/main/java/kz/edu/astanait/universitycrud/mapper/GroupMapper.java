package kz.edu.astanait.universitycrud.mapper;

import kz.edu.astanait.universitycrud.dto.response.GroupDtoResponse;
import kz.edu.astanait.universitycrud.model.Group;
import org.apache.logging.log4j.util.Strings;

public class GroupMapper {

    public static GroupDtoResponse groupToDto(Group group){
        GroupDtoResponse groupDtoResponse = new GroupDtoResponse();
        groupDtoResponse.setId(group.getId());

        if(Strings.isNotBlank(group.getName()))groupDtoResponse.setName(group.getName());
        return groupDtoResponse;
    }
}
