package kz.edu.astanait.universitycrud.mapper;

import kz.edu.astanait.universitycrud.dto.response.SpecialityDtoResponse;
import kz.edu.astanait.universitycrud.model.Speciality;
import org.apache.logging.log4j.util.Strings;


public class SpecialityMapper {

    public static SpecialityDtoResponse specialityToDto(Speciality speciality){
        SpecialityDtoResponse specialityDtoResponse = new SpecialityDtoResponse();

        specialityDtoResponse.setId(speciality.getId());
        if(Strings.isNotBlank(speciality.getName())) specialityDtoResponse.setName(speciality.getName());
        return specialityDtoResponse;
    }
}
