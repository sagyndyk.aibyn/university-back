package kz.edu.astanait.universitycrud.mapper;

import kz.edu.astanait.universitycrud.dto.response.TeacherDtoResponse;
import kz.edu.astanait.universitycrud.model.Teacher;
import org.apache.logging.log4j.util.Strings;

public class TeacherMapper {

    public static TeacherDtoResponse teacherToDto(Teacher teacher){
        TeacherDtoResponse teacherDtoResponse = new TeacherDtoResponse();

        teacherDtoResponse.setId(teacher.getId());
        if(Strings.isNotBlank(teacher.getName())) teacherDtoResponse.setName(teacher.getName());
        if(Strings.isNotBlank(teacher.getSurname())) teacherDtoResponse.setSurname(teacher.getSurname());
        return teacherDtoResponse;
    }
}
