package kz.edu.astanait.universitycrud.mapper;

import kz.edu.astanait.universitycrud.dto.response.StudentDtoResponse;
import kz.edu.astanait.universitycrud.model.Student;
import org.apache.logging.log4j.util.Strings;

public class StudentMapper {

    public static StudentDtoResponse studentToDto(Student student){
        StudentDtoResponse studentDtoResponse = new StudentDtoResponse();

        studentDtoResponse.setId(student.getId());
        if(Strings.isNotBlank(student.getName())) studentDtoResponse.setName(student.getName());
        if(Strings.isNotBlank(student.getSurname())) studentDtoResponse.setSurname(student.getSurname());
        return studentDtoResponse;
    }
}
