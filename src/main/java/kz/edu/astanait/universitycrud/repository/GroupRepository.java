package kz.edu.astanait.universitycrud.repository;

import kz.edu.astanait.universitycrud.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
}
