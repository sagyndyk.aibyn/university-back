package kz.edu.astanait.universitycrud.repository;

import kz.edu.astanait.universitycrud.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
}
