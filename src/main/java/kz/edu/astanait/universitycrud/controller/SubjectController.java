package kz.edu.astanait.universitycrud.controller;

import kz.edu.astanait.universitycrud.dto.request.SubjectDtoRequest;
import kz.edu.astanait.universitycrud.model.Subject;
import kz.edu.astanait.universitycrud.service.serviceInterface.SubjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/test/subject")
public class SubjectController {

    private final SubjectService subjectService;

    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subject> getById(@PathVariable(value = "id") Long id){
        Subject subject = this.subjectService.getByIdThrowException(id);
        return ResponseEntity.ok().body(subject);
    }

    @GetMapping("/")
    public List<Subject> getAll(){
        return this.subjectService.getAll();
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> create(@Valid @RequestBody SubjectDtoRequest subjectDtoRequest){
        this.subjectService.create(subjectDtoRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<HttpStatus> update(@RequestBody SubjectDtoRequest subjectDtoRequest,
                                             @PathVariable(name = "id") Long id){
        this.subjectService.update(subjectDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.subjectService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
