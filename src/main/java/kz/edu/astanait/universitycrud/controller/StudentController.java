package kz.edu.astanait.universitycrud.controller;

import kz.edu.astanait.universitycrud.dto.request.StudentDtoRequest;
import kz.edu.astanait.universitycrud.model.Student;
import kz.edu.astanait.universitycrud.service.serviceInterface.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/test/student")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Student> getById(@PathVariable(value = "id") Long id){
        Student student = this.studentService.getByIdThrowException(id);
        return ResponseEntity.ok().body(student);
    }

    @GetMapping("/")
    public List<Student> getAll(){
        return this.studentService.getAll();
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> create(@Valid @RequestBody StudentDtoRequest studentDtoRequest){
        this.studentService.create(studentDtoRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<HttpStatus> update(@RequestBody StudentDtoRequest studentDtoRequest,
                                             @PathVariable(name = "id") Long id){
        this.studentService.update(studentDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.studentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
