package kz.edu.astanait.universitycrud.controller;

import kz.edu.astanait.universitycrud.dto.request.GroupDtoRequest;
import kz.edu.astanait.universitycrud.model.Group;
import kz.edu.astanait.universitycrud.service.serviceInterface.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/test/group")
public class GroupController {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<Group> getById(@PathVariable(value = "id") Long id){
        Group group = this.groupService.getByIdThrowException(id);
        return ResponseEntity.ok().body(group);
    }

    @GetMapping("/")
    public List<Group> getAll(){
        return this.groupService.getAll();
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> create(@Valid @RequestBody GroupDtoRequest groupDtoRequest){
        this.groupService.create(groupDtoRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<HttpStatus> update(@RequestBody GroupDtoRequest groupDtoRequest,
                                             @PathVariable(name = "id") Long id){
        this.groupService.update(groupDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.groupService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
