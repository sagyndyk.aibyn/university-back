package kz.edu.astanait.universitycrud.controller;

import kz.edu.astanait.universitycrud.dto.request.SpecialityDtoRequest;
import kz.edu.astanait.universitycrud.model.Speciality;
import kz.edu.astanait.universitycrud.service.serviceInterface.SpecialityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/test/speciality")
public class SpecialityController {

    private final SpecialityService specialityService;

    public SpecialityController(SpecialityService specialityService) {
        this.specialityService = specialityService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Speciality> getById(@PathVariable(value = "id") Long id){
        Speciality speciality = this.specialityService.getByIdThrowException(id);
        return ResponseEntity.ok().body(speciality);
    }

    @GetMapping("/")
    public List<Speciality> getAll(){
        return this.specialityService.getAll();
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> create(@Valid @RequestBody SpecialityDtoRequest specialityDtoRequest){
        this.specialityService.create(specialityDtoRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<HttpStatus> update(@RequestBody SpecialityDtoRequest specialityDtoRequest,
                                             @PathVariable(name = "id") Long id){
        this.specialityService.update(specialityDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.specialityService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
