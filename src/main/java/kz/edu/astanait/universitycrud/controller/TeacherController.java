package kz.edu.astanait.universitycrud.controller;

import kz.edu.astanait.universitycrud.dto.request.TeacherDtoRequest;
import kz.edu.astanait.universitycrud.model.Teacher;
import kz.edu.astanait.universitycrud.service.serviceInterface.TeacherService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/test/teacher")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getById(@PathVariable(value = "id") Long id){
        Teacher teacher = this.teacherService.getByIdThrowException(id);
        return ResponseEntity.ok().body(teacher);
    }

    @GetMapping("/")
    public List<Teacher> getAll(){
        return this.teacherService.getAll();
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> create(@Valid @RequestBody TeacherDtoRequest teacherDtoRequest){
        this.teacherService.create(teacherDtoRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<HttpStatus> update(@RequestBody TeacherDtoRequest teacherDtoRequest,
                                             @PathVariable(name = "id") Long id){
        this.teacherService.update(teacherDtoRequest, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id){
        this.teacherService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
